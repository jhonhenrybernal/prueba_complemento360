function listPlayes(id) {
    var xmlHttp = new XMLHttpRequest();
    if (id == null) {
        
        xmlHttp.open("GET", 'http://pruebajs.com/json/players.json', false);
    }else{
        xmlHttp.open("GET", 'https://www.balldontlie.io/api/v1/players/'+id, false);
    }
    xmlHttp.send(null);
    return xmlHttp.responseText;
}

window.onload = function () {
    tableList()
}

function tableList(){
    var list = JSON.parse(listPlayes())
    
    let obj_unidos = list.data
    var tr;
    for (var i = 0; i < obj_unidos.length; i++) {
        tr = $('<tr/>');
        tr.append("<td>" + obj_unidos[i].id + "</td>");
        tr.append("<td>" + obj_unidos[i].first_name + "</td>");
        tr.append("<td>" + obj_unidos[i].last_name + "</td>");
        tr.append("<td>" + obj_unidos[i].position + "</td>");

        tr.append('<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" onClick="detailList(' + obj_unidos[i].id + ')" data-target="#exampleModalCenter" id="team" onclick="teamList()">Team details</button>');
        $('table').append(tr);
    }

}

function detailList(id){
    document.getElementById('alert_one').innerHTML = ''
    document.getElementById('alert').innerHTML = ''
    var team = searchData(id)
    
    var teamDom = document.getElementById('list-team')     
    teamDom.innerHTML = '<input type="hidden" id="id_players_view" value=" ' + team.id + '"><dt>id: ' + team.id + '</dt><dd><ul><li>Fisrt name: ' + team.first_name + ' </li><li>Laste name: ' + team.last_name + ' </li><li>Position: ' + team.position + ' </li><li>Abbreviation: ' + team.team.abbreviation + ' </li><li>City: ' + team.team.city + '</li><li>Conference: ' + team.team.conference + '</li><li>Division: ' + team.team.division + '</li><li>Full name: ' + team.team.full_name + '</li></li><li>Name: ' + team.team.name+'</li></ul></dd>'
}

function searchData(id){
    var teams = JSON.parse(listPlayes())
    var obj = teams.data
    for (var i = 0; i < obj.length; i++) {
        if (obj[i].id == id) {
            var team = obj[i]
        }
    }
    return team;
}

function createID(){
    var teams = JSON.parse(listPlayes())
    var obj = teams.data
    var aId = Math.floor(Math.random() * 100)
    for (var i = 0; i < obj.length; i++) {
        if (obj[i].id !== aId) {
            var teamId = aId
            break
        }
    }
    return teamId;
}


function searchFilter() {
    
    var input, filter, table, tr, td, cell, i, j;
    input = document.getElementById("first_name");
    filter = input.value.toUpperCase();
    table = document.getElementById("table");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {
        // Hide the row initially.
        tr[i].style.display = "none";

        td = tr[i].getElementsByTagName("td");
        for (var j = 0; j < td.length; j++) {
            cell = tr[i].getElementsByTagName("td")[j];
            if (cell) {
                if (cell.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                    break;
                }
            }
        }
    }
}

function newPlayers(){
    document.getElementById("form-players-modal").reset();
    document.getElementById('title_form').innerHTML = ''
    $('#new_player').modal().hide();
    document.getElementById('title_form').innerHTML = 'New Players'
}



function savePlayers() {

    const Http = new XMLHttpRequest();
    document.getElementById('alert').innerHTML = ''
    if (document.getElementById('title_form').innerHTML == 'Edit Players') {

        var player = {
            'id': document.getElementById('id_players_view').value,
            'first_name': document.getElementById('first_name_value').value,
            'last_name': document.getElementById('last_name').value,
            'position': document.getElementById('position').value,
            'team': {
                'id': Math.floor(Math.random() * 100),
                'abbreviation': document.getElementById('abbreviation').value,
                'city': document.getElementById('city').value,
                'conference': document.getElementById('conference').value,
                'division': document.getElementById('division').value,
                'full_name': document.getElementById('full_name').value,
                'name': document.getElementById('name').value
            },
        };
        var url = 'php/apiUpdate.php';
        var msm = 'Good! update'
    }
    if (document.getElementById('first_name_value').value == '' || document.getElementById('last_name').value == '' ) {
        alert('Insert values please')
    } else if (document.getElementById('title_form').innerHTML !== 'Edit Players'){
        var player = {
        'id': createID(),
        'first_name': document.getElementById('first_name_value').value,
        'last_name' : document.getElementById('last_name').value,
        'position' : document.getElementById('position').value,
            'team':{
                'id': Math.floor(Math.random() * 100),
                'abbreviation' : document.getElementById('abbreviation').value,
                'city' : document.getElementById('city').value,
                'conference' : document.getElementById('conference').value,
                'division' : document.getElementById('division').value,
                'full_name' : document.getElementById('full_name').value,
                'name' : document.getElementById('name').value
            },
        };
        var url = 'php/apiSave.php';
        var msm = 'Good! save'
    }

    Http.open("POST", url);
    Http.setRequestHeader("Content-type", "application/json")
    var data = JSON.stringify(player);

    Http.send(data);

    Http.onreadystatechange = (e) => {
        if (Http.status == 200) {
           
            document.getElementById('alert').innerHTML = '<div class="alert alert-success" role="alert">' + msm +'</div >'
            document.querySelectorAll("table tbody tr").forEach(function (e) { e.remove() })
            setTimeout(function () { tableList() }, 2000);
            
        }
        console.log(Http)
    }
   
}


function editPlayers(){
    var id = document.getElementById('id_players_view').value
    document.getElementById('title_form').innerHTML = ''

    $("#exampleModalCenter .close").click()
    $('#new_player').modal().hide();
    var team = searchData(id)
    document.getElementById('title_form').innerHTML = 'Edit Players'
    document.getElementById('first_name_value').value = team.first_name
    document.getElementById('last_name').value = team.last_name
    document.getElementById('position').value = team.position
    document.getElementById('abbreviation').value = team.team.abbreviation
    document.getElementById('city').value = team.team.city
    document.getElementById('conference').value = team.team.conference
    document.getElementById('division').value = team.team.division
    document.getElementById('full_name').value = team.team.full_name
    document.getElementById('name').value = team.team.name
}

function deletePlayers(){
    var id = document.getElementById('id_players_view').value
    
    confirm('Seguro?')
    var player = {'id': id};
    var data = JSON.stringify(player);
    const Http = new XMLHttpRequest();
    Http.open("POST", 'php/apiDelete.php');
    Http.setRequestHeader("Content-type", "application/json")
    Http.send(data);
    setTimeout(function () { tableList() }, 1000);
    Http.onreadystatechange = (e) => {
        if (Http.status == 200) {

            document.getElementById('alert_one').innerHTML = '<div class="alert alert-danger" role="alert">Removed</div>'
            document.querySelectorAll("table tbody tr").forEach(function (e) { e.remove() })
            setTimeout(function () { tableList() }, 1000);

        }
        console.log(Http)
    }

}