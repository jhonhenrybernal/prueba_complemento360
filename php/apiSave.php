<?php
$file = dirname(__FILE__);
$file_proyect = realpath('./../');
 
$json_old = file_get_contents($file_proyect . '\json\players.json');
$data = json_decode($json_old, true);
$merge = $data['data'];

$json_new = file_get_contents('php://input');
$merge[] = json_decode($json_new,true);
$json_create = array(
    'data' => $merge
);
$json = json_encode($json_create);

$fp = fopen($file_proyect.'\json\players.json', 'w');
fwrite($fp, $json);
fclose($fp);

return json_encode(true);