<?php
$json_new = file_get_contents('php://input');
$request = json_decode($json_new, true);

$id = $request['id'];

$file_proyect = realpath('./../');
$jsonFile = file_get_contents($file_proyect . '\json\players.json');
$jsonDeco = json_decode($jsonFile, true);
$data = $jsonDeco['data'];
foreach ($data as $key => $value) {
    $n1 = intval($data[$key]['id']);
    $n2 = intval($id);
    //var_dump($n1 === $n2);die;
    if ($n1 == $n2) {
        $data[$key]['first_name'] = $request['first_name'];
        $data[$key]['last_name']=$request['last_name'];
        $data[$key]['position']= $request['position'];
        $data[$key]['team']['position']=['abbreviation'];
        $data[$key]['team']['position']=['city'];
        $data[$key]['team']['position']=['conference'];
        $data[$key]['team']['position']=['division'];
        $data[$key]['team']['position']=['full_name'];
        $data[$key]['team']['position']=['name'];
    
    }
}
$json_create = array(
    'data' => $data
);
$final_data = json_encode($json_create);
$fp = fopen($file_proyect . '\json\players.json', 'w');
fwrite($fp, $final_data);
fclose($fp);