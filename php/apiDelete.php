<?php
$json_new = file_get_contents('php://input');
$request = json_decode($json_new, true);

$id = $request['id'];


$file_proyect = realpath('./../');
$jsonFile = file_get_contents($file_proyect . '\json\players.json');
$jsonDeco = json_decode($jsonFile, true);
$data = $jsonDeco['data'];

// get array index to delete
$arr_index = array();
foreach ($data as $key => $value) {
    if ($value['id'] == $id) {
        $arr_index[] = $key;
    }
}
/*echo "<pre>";
print_r($data);
echo "<pre>";
print_r($arr_index);die;*/
// delete data
foreach ($arr_index as $i) {
    unset($data[$i]);
}

// rebase array
$data = array_values($data);


$json_create = array(
    'data' => $data
);
$final_data = json_encode($json_create);
$fp = fopen($file_proyect . '\json\players.json', 'w');
fwrite($fp, $final_data);
fclose($fp);